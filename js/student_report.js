// var student;
var searchTerm;

var searchIndexOf = function(searchTerm, searchTarget) {
  var results = [];
  for (var i = 0; i < searchTarget.length; i += 1) {
    var student = searchTarget[i];
    if (searchTerm.toLowerCase() === student.name.toLowerCase()) {
      results.push(i);
    }
  }
  if (results.length === 0) {
    console.log(-1);
    return -1;
  } else {
    console.log(results);
    return results;
  }
};

var generateOutputByIndex = function(resultsIndex) {
  var html = "";
  if (resultsIndex === -1) {
    html += "Error: student not found."
  } else {
    for (var i = 0; i < resultsIndex.length; i++) {
      var index = resultsIndex[i];
      var target = students[index]
      console.log(index);
      console.log(target);
      html += "<p><strong>Student Record</strong><br>";
      for (var key in target) {
        html += key.toUpperCase() + ": " + target[key] + "<br>";
      }
    }
  }
  html += "</p>"
  $("#output").html(html);
};

var queryUser = function () {
    searchTerm = prompt("Search Target:");
};

while (true) {
  queryUser();
  if (searchTerm === null || searchTerm.toLowerCase() === "quit") {
    break;
  } else {
    generateOutputByIndex(searchIndexOf(searchTerm, students));
  }
}
